# Source tree link with GitLab

1. Add the public key to GitLab

![](../.gitbook/assets/image%20%282%29.png)

2. Open PuTTY authentication agent

![](../.gitbook/assets/image%20%2872%29.png)

3. Add the private key to pageant key list

![](../.gitbook/assets/image%20%2815%29.png)

4. Now can clone from GitLab

![](../.gitbook/assets/image%20%2874%29.png)

