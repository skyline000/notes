# One-time Pad Decryption

**Encryption**: Plaintext `XOR` Key = Ciphertext

![](../.gitbook/assets/image%20%2864%29.png)



**Decryption**: Ciphertext `XOR` Key = Plaintext

![](../.gitbook/assets/image%20%2860%29.png)

* Pad must be random, **used only once** 
* Pad has the same size as message



Good: The ciphertext can decrypt to any possible plaintext 

Bad: Managing the key \(the pad\) is not practical

