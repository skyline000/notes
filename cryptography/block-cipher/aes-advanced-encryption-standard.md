# AES \(Advanced Encryption Standard\)

## **Four Transformations**

* ByteSub
* ShiftRow
* MixColumn
* AddRoundKey

![](../../.gitbook/assets/image%20%2835%29.png)

![](../../.gitbook/assets/image%20%2830%29.png)

![](../../.gitbook/assets/image%20%2841%29.png)



![](../../.gitbook/assets/image%20%2859%29.png)



### ByteSub

![](../../.gitbook/assets/image%20%2850%29.png)

![](../../.gitbook/assets/image%20%2876%29.png)



### ShiftRow

![](../../.gitbook/assets/image%20%287%29.png)



### MixColumn

![](../../.gitbook/assets/image%20%2818%29.png)

![](../../.gitbook/assets/image%20%2869%29.png)

![](../../.gitbook/assets/image%20%2836%29.png)

![](../../.gitbook/assets/image%20%2875%29.png)



### AddRoundKey

![](../../.gitbook/assets/image%20%2829%29.png)



## Summary of One AES Round \(except the last round\)

![](../../.gitbook/assets/image%20%2856%29.png)















